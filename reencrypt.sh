#!/bin/sh

image=$1 loop=$( kpartx -av ${image} | awk 'NR==1{ print substr($3,1,5) }' )
echo -n "123456" | cryptsetup-reencrypt /dev/mapper/${loop}p2 -
sync
losetup -d /dev/${loop}
kpartx -d /dev/${loop}
